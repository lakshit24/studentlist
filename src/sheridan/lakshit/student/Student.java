package sheridan.lakshit.student;
/**
 * Student.java
 * @author lakshit
 */
public class Student
{

   private String name;
   private String id;
   private String program ;

   public Student (String name, String id)
   {
      this.name = name;
      this.id = id;
   }

   public String getName ()
   {
      return name;
   }

   public void setName (String name)
   {
      this.name = name;
   }

   public String getId ()
   {
      return id;
   }

   public void setId (String id)
   {
      this.id = id;
   }

    /**
     * @return the program
     */
    public String getProgram() {
        return program;
    }

    /**
     * @param program the program to set
     */
    public void setProgram(String program) {
        this.program = program;
    }
}
