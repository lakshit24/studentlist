package sheridan.lakshit.student;

import java.util.Scanner;

/**
 * StudentList.java
 *
 * @author lakshit
 */
public class StudentList
{

   /**
    * @param args the command line arguments
    */
   public static void main (String[] args)
   {
      
      Student[] students = new Student[2];

      Scanner sc = new Scanner(System.in);

      for (int i = 0; i < students.length; i++) {

         System.out.println("Enter name of student");
         String name = sc.nextLine();

         System.out.println("Enter the sheridan id of student");
         String id = sc.nextLine();

         Student student = new Student(name, id);

         students[i] = student;
      }

      System.out.println("Printing the students:");

      String format = "The name of student and their id is: %s\n";

      for (Student student : students) {

         System.out.printf(format, student.getName());
         System.out.println(student.getId());
      }
   }
}

